import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

var firebaseConfig = {
  apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
  authDomain: "winnerenglish-5f8d3.firebaseapp.com",
  databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
  projectId: "winnerenglish-5f8d3",
  storageBucket: "winnerenglish-5f8d3.appspot.com",
  messagingSenderId: "40516029824",
  appId: "1:40516029824:web:f5e3d3a9583427c9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();



Vue.mixin({
  data() {
    return {
      appVersion: "1.0.0",
    }
  },
  methods: {
    showLoading(text) {
      this.$q.loading.show({
        delay: 400, // ms
        message: text
      });
    },
    hideLoading() {
      setTimeout(() => {
        this.$q.loading.hide();
      }, 500);
    },
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: 'top',
        icon: "error",
        message: messages,
        timeout: 800,
      });
    },
  },


});



export default function ( /* { store, ssrContext } */ ) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
