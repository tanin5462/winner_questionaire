const routes = [{
    path: '/',
    component: () => import('pages/login.vue'),
    name: "Login"
  },
  {
    path: '/questionaire',
    component: () => import('pages/questionaire.vue'),
    name: "quetsionaire"
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
